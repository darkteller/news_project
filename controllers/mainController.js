const News = require('../models/news');
const mongoose = require('mongoose');

exports.newsHomePage = (req,res) => {
    res.redirect('/news');
}
exports.HomePage = (req, res) => {
    News.find({}, (err, allNews) => {
        if(err){
            console.log(err);
        }else{
            res.render('news', {news: allNews});                 
        }
    });
 }
exports.pageAdd = (req, res) => {
    res.render('add_news');
};
exports.addNews = (req, res) => {
    const {title, image, desc} = req.body;
    const newNews = {title, image, desc};
    if (!req.files)
    return res.status(400).send('No files were uploaded.');
    let sampleFile = req.files.sampleFile;
    var fileName = req.body.title;
    sampleFile.mv(__dirname +'/../public/uploads/'+ fileName +'.jpg', function(err) {
    if (err)
      return res.status(500).send(err);
    News.create(newNews, (err, newlyCreated) => {
        if(err){
            console.log(err);
        }else{
            res.redirect('/news');
        }       
    });
});
}
exports.singlePage = (req, res) => {
    News.findOne({_id: req.params.id}, (err, foundNews) => {
        if(err){
            console.log(err);
        }else{
            res.render('single_news', {news: foundNews});
        }
    });
}
exports.showEditPage = (req, res) =>{
    News.findOne({_id: req.params.id}, (err, foundNews) => {
        if(err){
            console.log(err);
        }else{
            res.render('edit', {news: foundNews});
        }
    });
}

exports.editPage = (req, res) => {
    News.updateOne({_id: req.params.id}, req.body, (err) =>{
        if(err) {
            res.redirect('/news');
        }else{
            res.redirect('/news/' + req.params.id);
        }
    });
};
exports.deletedPost = (req, res) => {
    News.deleteOne({_id: req.params.id}, (err) => {
       if(err){
           res.redirect("/news");
       } else {
           res.redirect("/news");
       }
    });
 };
 
 