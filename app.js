const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const methodOverride = require("method-override");
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const routes = require('./routes/index');


const app = express();


mongoose.connect('mongodb://root:root@ds137464.mlab.com:37464/spa');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());
app.use(methodOverride("_method"));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

app.listen(3000, () =>{
    console.log('server start');
});

