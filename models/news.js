var mongoose = require("mongoose");
 
var newsSchema = new mongoose.Schema({
   title: String,
   image: String,
   desc: String,
});
 
module.exports = mongoose.model("News", newsSchema);