const express = require('express');
const router = express.Router();
const mainController = require('../controllers/mainController');

router.get('/', mainController.newsHomePage)
router.get('/news', mainController.HomePage);
router.get('/news/add-news', mainController.pageAdd)
router.post('/news/add-news', mainController.addNews);
router.get('/news/:id', mainController.singlePage);
router.get('/news/:id/edit', mainController.showEditPage)
router.post('/news/:id', mainController.editPage);
router.delete('/news/:id', mainController.deletedPost);

module.exports = router;